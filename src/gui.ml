open Client_state
open Config
open GMain
open GdkKeysyms
open Gui_common
open Gui_dialog
open Gui_file
open Move
open Utils

let (//) = Filename.concat

let font_name = "fixed"
(* let font_name = "-*-helvetica-medium-r-normal-*-120-*" *)

(* True when this is the turn of a human player *)
let click_request = ref false

(* Position of the current selected penguin *)
let selected = ref (-1,-1)

(* Portals used in the current displacement (pos * pos) list *)
let portals = ref []
(* Direction when portals are used *)
let cur_move = ref (Move.E,-1)

(* List of blocked players (no more possible movements) *)
let blocked_players = ref []


(* TODO: erreur différente *)
let font = try
    Gdk.Font.load font_name
  with
    Gpointer.Null -> failwith ("gui: font " ^ font_name ^ ": not found")

(* Drawing area to display the game board *)
let da = GMisc.drawing_area ()

(* Background drawing *)
let backing = GDraw.pixmap ~height:10000 ~width:10000 ()

(** handle quit signal, ask to save game if necessary *)
let quit () =
  _log "Goodbye!";
  Main.quit()

(* TODO gestion d'erreur, assert sur les tailles d'image *)
let pixbuf_ice = GdkPixbuf.from_file (path_img // "ice.png")
let pixbuf_water = GdkPixbuf.from_file (path_img // "water.png")
let pixbuf_penguin = GdkPixbuf.from_file (path_img // "penguin.png")
let pixbuf_current = GdkPixbuf.from_file (path_img // "penguin_s.png")
let pixbuf_selected = GdkPixbuf.from_file (path_img // "penguin_c.png")
let pixbuf_nostop = GdkPixbuf.from_file (path_img // "nostop.png")
let pixbuf_teleport = GdkPixbuf.from_file (path_img // "teleport.png")
let pixbuf_teleport_s = GdkPixbuf.from_file (path_img // "teleport_s.png")
let pixbuf_welcome = GdkPixbuf.from_file (path_img // "welcome.jpg")

let blocked pos =
  let gs = get_gs() in
  List.for_all
    (fun d -> let (b,_,_) = gs#legal_move_n pos (d,1) in not b)
    Move.all_directions

(* Callback to repaint the drawing area *)
let expose event =
  let area = GdkEvent.Expose.area event in
  let x = Gdk.Rectangle.x area
  and y = Gdk.Rectangle.y area
  and width = Gdk.Rectangle.width area
  and height = Gdk.Rectangle.height area in
  da#misc#realize ();
  let drawing = new GDraw.drawable da#misc#window in
  drawing#put_pixmap ~x ~y ~xsrc:x ~ysrc:y ~width ~height backing#pixmap;
  false

(* atualize the text (scores + challenges) behind the game board *)
let update_text() =
  let gs = get_gs() in
  let (m,_) = gs#dimensions () in

  let max_name_len = Array.fold_left
                       (fun m p -> max m (String.length p#get_name)) 0
                       (gs#get_players())  in

  let context = da#misc#create_pango_context in
  let layout = context#create_layout in
  (* Display scores *)

  Array.iteri (fun i p ->
      Pango.Layout.set_text layout p#get_name;
      backing#put_layout ~x:(20) ~y:(m*52+i*20)
                         ~fore:(if gs#get_turn() = i
                                then (`NAME "#0000ff")
                                else `BLACK) layout;
      Pango.Layout.set_text layout (string_of_int p#get_score);
      backing#put_layout ~x:(20+max_name_len*10) ~y:(m*52+i*20)
                         ~fore:(if gs#get_turn() = i
                                then (`NAME "#0000ff")
                                else `BLACK) layout;
    ) (gs#get_players());

  (* Display challenges values *)
  List.iteri (fun i ch ->
      let p = (gs#get_players()).(0) in
      let s = match ch with
        (* TODO *)
      | MAX_FISH v -> Printf.sprintf "Poissons restants à manger : %d"
                                     (v - p#get_score)
      | MIN_FISH v -> Printf.sprintf "Poissons avant de perdre : %d"
                                     (v - p#get_score)
      | MIN_TELEPORT v -> Printf.sprintf "Téléportations restantes : %d"
                                         (v - p#teleport_counter)
      | MIN_MOVE v -> Printf.sprintf "Mouvements restants : %d"
                                     (v - p#turn_counter)
      in
      Pango.Layout.set_text layout s;
      backing#put_layout ~x:(70+max_name_len*10) ~y:(m*52+i*20) layout;
    ) gs#challenges

(* actualize the board *)
let update_board() =
  let width, height = backing#size in
  backing#set_foreground (`NAME "#eeeeee"); (* grey for background *)
  backing#rectangle ~x:0 ~y:0 ~width ~height ~filled:true ();
  backing#set_foreground `BLACK; (* black for text *)
  let gs = get_gs() in
  let (m,n) = gs#dimensions () in
  da#set_size ~height:(50*m+35+(12* (max (Array.length (gs#get_players()))
                                         (List.length (gs#challenges)))))
              ~width:(56*n);

  (* array: positions of the current player's penguins *)
  let pen_pos = (gs#get_players())
                .(gs#get_turn())#get_penguins_pos in

  let draw_pen i j x y =
    if (i,j) = !selected
    then backing#put_pixbuf ~x ~y pixbuf_selected
    else if Array.mem (i,j) pen_pos && not (blocked (i,j))
    then backing#put_pixbuf ~x ~y pixbuf_current
    else backing#put_pixbuf ~x ~y pixbuf_penguin
  in

  (* remember portals encountered to display correct letters *)
  let teleport = ref [] in

  for i = 0 to m - 1 do
    for j = 0 to n - 1 do
      let x = j * 53 + (if i mod 2 = 1 then 0 else 26) in
      let y = i * 46 in
      match gs#get_cell i j with
      | ICE(v) ->  backing#put_pixbuf ~x ~y pixbuf_ice;
                   backing#string (string_of_int v) ~font ~x:(x+29) ~y:(y+37)
      | WATER -> backing#put_pixbuf ~x ~y pixbuf_water;
      | PENGUIN -> draw_pen i j x y
      | NOSTOP -> backing#put_pixbuf ~x ~y pixbuf_nostop
      | TELEPORT (dest,busy) ->
         let n =
           try List.assoc (i,j) !teleport
           with _ -> teleport := (dest,(List.length !teleport))::!teleport;
                     List.length !teleport - 1
         in
         if busy then draw_pen i j x y else
           begin
             if (List.exists (fun (p,p') -> (i,j) = p || (i,j) = p') !portals)
             then backing#put_pixbuf ~x ~y pixbuf_teleport_s
             else backing#put_pixbuf ~x ~y pixbuf_teleport;
             (* display portal couples with letters *)
             backing#string (String.make 1 (Char.chr (n + (Char.code 'A'))))
                            ~font ~x:(x+29) ~y:(y+37)
           end
    done;
  done;
  update_text()

let challenge_finished () =
  let ok = ref false in
  let gs = get_gs() in
  List.iteri (fun i ch ->
      let p = (gs#get_players()).(0) in
      match ch with
      | MAX_FISH v
        | MIN_FISH v -> if (v - p#get_score) <= 0 then ok := true
        | MIN_TELEPORT v -> if (v - p#teleport_counter) < 0 then ok := true
        | MIN_MOVE v -> if (v - p#turn_counter) <= 0 then ok := true
    ) gs#challenges;
  !ok

(* play until the turn of a human player *)
let rec play () =
  let gs = get_gs() in
  let players = gs#get_players() in
  let turn = gs#get_turn() in
  update_board();
  let player = players.(turn) in
  if challenge_finished()
  then st_push "Partie terminée"
  else if Array.for_all blocked player#get_penguins_pos
  then (
    if not (List.mem turn !blocked_players)
    then blocked_players := turn::!blocked_players;
    if List.length !blocked_players = Array.length(gs#get_players())
    then st_push "Partie terminée"
    else begin
        gs#next_turn();
        play()
      end
  ) else (
    st_push ("Au tour du joueur " ^ player#get_name);
    if player#is_human
    then (
      selected := (-1,-1);
      portals := [];
      cur_move := (Move.E,-1);
      click_request := true
    ) else (
      (try
         gs#move_player_cpu player#get_name;
       with e ->
         st_push "L'IA s'est plantée. Pas grave, relancez une partie";
         _log ("IA exception: " ^ (Printexc.to_string e));
         failwith "IA Exception");
      gs#next_turn();
      play()
    )
  )

(* attempt automatic end-game evaluation *)
let end_game() =
  let gs = get_gs() in
  if (List.length gs#challenges) > 0
  then st_flash ~delay:2000 "Petit malin"
  else if !click_request then
    begin
      let module Grid : Paths.S = struct
          let grid = gs#get_map ()
        end in
      let module Path = Paths.Make (Grid) in
      let player = (gs#get_players()).(gs#get_turn()) in
      let pen_pos = Array.to_list player#get_penguins_pos in
      List.iter (fun pos ->
          let acc = Path.grid_of_set (Path.accessible gs Path.grid_set pos) in
        (* TODO *) ()
        ) pen_pos;
      ()
    end

(* return the Move.move from initial position to destination *)
(* invalid moves have second component equal to -1 *)
let move_of_pos (i_s,j_s) (i_d,j_d) =
  (* Printf.printf "(%d,%d)(%d,%d)\n%!" i_s j_s i_d j_d; *)
  if i_s = -1 || i_d = -1 then
    (Move.E,-1)
  else if i_s = i_d
  then ((if j_s < j_d then Move.E else Move.W), (abs (j_s - j_d)))
  else let dy = i_s - i_d and dx = j_s - j_d in
    let x = abs dx and y = abs dy in
    (* Printf.printf "%d %d %d %d\n%!" dy y dx x; *)
    if i_s mod 2 = 0
    then
      if x = y/2 && dx >= 0
      then ((if dy > 0 then Move.NW else Move.SW),y)
      else if x = (y+1)/2 && dx < 0
      then ((if dy > 0 then Move.NE else Move.SE),y)
      else (Move.E,-1)
    else
    if x = (y+1)/2 && dx > 0
    then ((if dy > 0 then Move.NW else Move.SW),y)
    else if x = y/2 && dx <= 0
    then ((if dy > 0 then Move.NE else Move.SE),y)
    else (Move.E,-1)

(* return the grid coordinates from mouse click coordinates *)
(* and (-1,-1) for coordinates out of the grid *)
let mouse_to_coord (x,y) =
  let (m,n) = (get_gs())#dimensions() in
  let mouse_to_coord_ (x,y) =
    if (x < 6 || y < 3)
    then (-1,-1)
    else
      let di = (y-3) / (46*2) in
      let dj = (x-6) / 53 in
      let i = (y-3) mod (46*2) in
      let j = (x-6) mod 53 in
      (* cut vertically the figure *)
      let b = j < 26 in
      let jj = if b then j else 53-j in (* symmetry *)
      if i <= (jj *13)/26
      then                (* North *)
        (if di > 0 then (di*2-1, dj) else (-1,-1))
      else if i >= 54 - (jj * 13)/26
      then                (* South *)
        (if di < m-1 then (di*2+1, dj) else (-1,-1))
      else (if b then     (* West *)
              (if dj > 0 then (di*2, dj-1) else (-1,-1))
            else          (* Here *)
              (di*2,dj))
  in
  let (i,j) = mouse_to_coord_ (x,y) in
  if i >= m || j >= n then (-1,-1) else (i,j)

(* simple check to avoid sending weird movement to legal_move_n *)
let first_check dest =
  let gs = get_gs() in
  let p = match !portals with
    | [] -> !selected
    | (_,q')::_ -> q'
  in
  let p' = ref p in
  let e = Invalid_argument "Mouvement invalide" in
  let (d,n) = (move_of_pos p dest) in
  if n = -1 then raise e;
  for _ = 1 to n - 1 do
    p' := Move.move !p' d;
    match gs#get_cell (fst !p') (snd !p') with
    | ICE _ | NOSTOP -> ()
    (* we can stay on a portal when the other is busy *)
    | TELEPORT ((i,j),false) ->
       (match gs#get_cell i j
        with TELEPORT (_,true)-> ()
           | _ -> raise e)
    | _ -> raise e
  done;
  ()

let clean_next_turn gs =
  selected := (-1,-1);
  portals := [];
  cur_move := (Move.E,-1);
  click_request := false;
  gs#next_turn()


(* handle left click on the game board *)
let click ev =
  let x = int_of_float (GdkEvent.Button.x ev) in
  let y = int_of_float (GdkEvent.Button.y ev) in
  let (i,j) = mouse_to_coord (x,y) in
  (* Printf.printf "(%d,%d)\n%!" i j; *)
  let gs = get_gs() in

  (try
    if not !click_request
    then raise (Invalid_argument "Veuillez attendre votre tour");

    let player = (gs#get_players()).(gs#get_turn()) in
    let pen_pos = player#get_penguins_pos in
    if Array.mem (i,j) pen_pos && not (blocked (i,j))
    then (selected := (i,j);
          portals := [];
          cur_move := (Move.E,-1);
          update_board())
    else
      if !selected = (-1,-1)
      then raise (Invalid_argument "Veuillez choisir un de vos manchots")
      else
        match first_check (i,j); gs#get_cell i j with
        | TELEPORT ((k,l) as dst, false) when
               not (List.mem_assoc (i,j) !portals) -> begin
            let busy =
              match  gs#get_cell k l with
              | TELEPORT (_,b) -> b
              | _ -> failwith "Impossible"
            in
            match !portals with
            | [] ->
               if busy then (
                 gs#move_player player#get_name !selected
                                (move_of_pos !selected (i,j));
                 clean_next_turn gs;
                 play()
               ) else (
                 portals := [((i,j),dst)];
                 cur_move := move_of_pos !selected (i,j);
                 update_board()
               )
            | (_,p')::_ ->
               if (i,j) = p'
               then begin     (* the penguin stops on the out portal *)
                   gs#move_player player#get_name !selected !cur_move;
                   clean_next_turn gs;
                   play()
                 end
               else
                 let (cur_dir,cur_n) = !cur_move in
                 let (d,n) = move_of_pos p' (i,j) in
                 if (n != -1 && d = cur_dir)
                 then begin
                     portals := ((i,j),dst)::!portals;
                     cur_move := (d, n + cur_n);
                     if busy then (
                       gs#move_player player#get_name !selected !cur_move;
                       clean_next_turn gs;
                       play()
                     )
                     else update_board()
                   end
          end
        | _ ->
           let mov = match !portals with
             | [] -> (move_of_pos !selected (i,j))
             | (_,p')::_ ->
                let (cur_dir,cur_n) = !cur_move in
                let (d,n) = (move_of_pos p' (i,j)) in
                if (n != -1 && d = cur_dir)
                then (d,n+cur_n)
                else (d,-1)
           in
           gs#move_player player#get_name !selected mov;
           clean_next_turn gs;
           play()
   with
     Invalid_argument s -> st_flash ~delay:3000 s);
  true

(* handle mouse clicks on the game board *)
let button_pressed ev =
  if GdkEvent.Button.button ev = 1
  then click ev
  else true (* no right click or other event*)

(* load from existing object game_state in gs *)
let real_load_game() =
  let state = get_gs() in
  window#set_title (state#name);
  _log "loading finished";
  ignore (da#event#connect#button_press ~callback:(button_pressed));
  ignore (da#event#connect#expose ~callback:expose);
  blocked_players := [];
  portals := [];
  selected := (-1,-1);
  cur_move := (Move.E,-1);
  play()


(* draw an image on the initial screen *)
let expose_welcome _ =
  da#misc#realize ();
  let drawing = new GDraw.drawable da#misc#window in
  drawing#put_pixbuf ~x:0 ~y:0 pixbuf_welcome;
  false

let main() =
  (* warning -48 because we like implicit elimination of optional arguments *)

  load_game := real_load_game;
  ignore (window#connect#destroy ~callback:quit);

  let vbox = GPack.vbox ~packing:window#add () in

  (* Menu bar *)
  let menubar = GMenu.menu_bar ~packing:vbox#pack () in
  let factory = new GMenu.factory menubar in
  let accel_group = factory#accel_group in
  let file_menu = factory#add_submenu "_Fichier" in
  let multi_menu = factory#add_submenu "_Multijoueur" in
  let help_menu = factory#add_submenu "_Aide" in

  (* File menu *)
  let factory = new GMenu.factory file_menu ~accel_group in
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`NEW ())#coerce in
  ignore (factory#add_image_item
            ~image:icon ~label:"_Nouveau jeu" ~key:_N
            ~callback:(fun () -> new_game false) ());
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`OPEN ())#coerce in
  ignore (factory#add_image_item
            ~image:icon ~label:"_Charger une partie" ~key:_O
            ~callback:chose_game ());
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`QUIT ())#coerce in
  ignore (factory#add_image_item
            ~image:icon ~label:"_Quitter" ~key:_Q ~callback:quit ());
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`REFRESH ())#coerce in
  ignore (factory#add_image_item
            ~image:icon ~label:"_Recharger" ~key:_R ~callback:reload_game());

  (* Multi menu *)
  let factory = new GMenu.factory multi_menu ~accel_group in
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`NEW ())#coerce in
  ignore (factory#add_image_item
            ~image:icon ~label:"_Créer" ~key:_M
            ~callback:(fun () -> new_game true)());
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`NETWORK ())#coerce in
  ignore (factory#add_image_item
            ~image:icon ~label:"_Rejoindre" ~callback:join_game ());

  (* Help menu *)
  let factory = new GMenu.factory help_menu ~accel_group in
  let doc_url = " https://gitlab.com/softwa/very-hungry-penguins/blob/\
                 master/documentation.md" in
  let open_doc() = ignore (Unix.system ("xdg-open "^doc_url)) in
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`EXECUTE ())#coerce in
  ignore (factory#add_image_item ~image:icon ~label:"_Fin de partie"
                                 ~callback:end_game ());
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`HELP ())#coerce in
  ignore (factory#add_image_item ~image:icon ~label:"_Documentation"
                                 ~callback:open_doc ());
  let icon = (GMisc.image ~icon_size:`MENU ~stock:`ABOUT ())#coerce in
  ignore (factory#add_image_item ~image:icon ~label:"À _propos" ());

  (* automatic scrolling bars *)
  let scroll = GBin.scrolled_window
      ~hpolicy:`AUTOMATIC ~vpolicy:`AUTOMATIC
      ~packing:vbox#add () in

  da#event#add [`BUTTON_PRESS];
  (* pack the drawing area *)
  scroll#add_with_viewport da#coerce;

  (* add the status bar to the bottom of the main window *)
  vbox#pack status#coerce;
  st_push "Bienvenue dans notre jeu !";

  (* Display the windows and enter Gtk+ main loop *)
  window#add_accel_group accel_group;
  window#show ();

  (* game_state already initialized in client.ml *)
  if !filename <> "" then real_load_game()
  else ignore (da#event#connect#expose ~callback:expose_welcome);

  GMain.main ()
